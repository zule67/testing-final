import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { ITask } from 'src/app/Models/ITask';
import { TaskService } from 'src/app/Services/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  // Lista de contactos que se rellena con los datos del servicio
  tasks: ITask[] = [];
  createTask: FormGroup;

  constructor( public service: TaskService,
                public fb: FormBuilder,
              ) { 
                this.createTask = this.fb.group({
                  id: ['', Validators.required],
                  user_id: ['', Validators.required],
                  title: ['', Validators.required],
                  due_on: ['', Validators.required],
                  status: ['', Validators.required]
              })
              }

  ngOnInit(): void {
    this.service.getTasks().subscribe((respuesta) => {
      this.tasks = respuesta.data;
    },
    (error) => alert(`Ha ocurrido un error al obtener las tareas: ${error}`),
    () => console.log('Lista de tareas obtenida con éxito')
    )
  }

  addTask() {
    if(this.createTask.valid) {
      let nuevaTarea: ITask = { ...this.createTask.value }
      this.tasks.push(nuevaTarea);
      console.table(nuevaTarea);
    }
  }

  deleteTask(task: ITask) {
    const index = this.tasks.findIndex(
      (element: ITask) => element.id === task.id
    );
    this.tasks.splice(index, 1);
  }
}
