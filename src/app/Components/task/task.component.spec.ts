import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TaskComponent } from './task.component';
import { FormsModule, ReactiveFormsModule, NgControl } from '@angular/forms';
import { ITask } from 'src/app/Models/ITask';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TaskService } from 'src/app/Services/task.service';
import { TaskServiceStub } from '../Mocks/taskMock';
import { By } from '@angular/platform-browser';

describe('TaskComponent', () => {
  let component: TaskComponent;
  let fixture: ComponentFixture<TaskComponent>;
  let taskService: TaskService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskComponent ],
      imports: [
        HttpClientModule,
        //HttpClient,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        //HttpClientTestingModule,
        //HttpTestingController,
      ],
      providers: [
        { 
          provide: TaskService, 
          useClass: TaskServiceStub 
        }
      ]
    })
    .compileComponents();
    //taskService = TestBed.inject(TaskService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Retorna un formulario inválido', () => {
    const fixture = TestBed.createComponent(TaskComponent);
    const app = fixture.componentInstance;
    fixture.detectChanges();

    const form = app.createTask;
    const id = app.createTask.controls['id'];
    id.setValue('11');
    expect(form.invalid).toBeTrue();
  });

  it('Llama a getTasks del Servicio en ngOnInit', () => {
    
    spyOn(component.service, 'getTasks').and.callThrough();

    component.ngOnInit();
    expect(component.service.getTasks).toHaveBeenCalledTimes(1);
  });

  it('Obtiene del servicio la lista de tareas', () => {
    fixture.whenStable().then(() => {
      expect(component.tasks.length).toBeGreaterThan(0);
    })
  })

  it('Renderiza la lista de tareas en el HTML', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      
      const elemento = fixture.debugElement.query(By.css('.tasks')).nativeElement;
      
      expect(elemento.childNodes[1]).not.toBeNull();
    })
  });

});
