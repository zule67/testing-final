export interface ITask {
    id: number,
    user_id: string,
    title: string,
    due_on: Date,
    status: string
}

//Token Netlify: 
//d8e51057-9328-481d-bdd6-a6e5e4cb1747 --auth dR8LjsV3rg9sZfUCPNK_OdmMIgzwYXSLY5ZDEW1NG0A --prod",
//$NETLIFY_SITE-ID --auth $NETLIFY_AUTH_TOKEN --prod"