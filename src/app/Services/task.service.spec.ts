import { TestBed, getTestBed, fakeAsync } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing'; 
import { TaskService } from './task.service';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { TaskServiceStub } from '../Components/Mocks/taskMock';

describe('TaskService', () => {
  let service: TaskService;
  let taskMock: TaskServiceStub;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    service = TestBed.inject(TaskService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Carga los datos desde el servidor', fakeAsync( () => {
    const service = TestBed.inject(TaskService);
    spyOn(service, 'getTasks');
  }));

});
