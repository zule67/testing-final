import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ITask } from '../Models/ITask';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(public http: HttpClient) { }

  getTasks(): Observable<any> {

  //Hacer la peticion HTTP a gorest.co.in y devolver un Observable con la lista de usuarios
    return this.http.get<ITask[]>('https://gorest.co.in/public/v1/todos', {responseType:'json'});
  }
}
