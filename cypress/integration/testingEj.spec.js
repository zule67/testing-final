describe('Comprobación de crear y borrar tareas', () => {

    beforeEach(()=>{
        cy.visit('/');
      })

    it('Comprobar botón crear', () => {
        cy.get('#mainId').type(2);
        cy.get('#userId').type("2");
        cy.get('#titleId').type("Hola Carlos");
        cy.get('#dateId').type('2021-09-02');
        cy.get('#statusId').type("pending");

        cy.get('#btnCreate').click();

      });

      it('Comprobar botón borrar', () => {
          cy.get('#btnDelete').click();
        
    });
    
})